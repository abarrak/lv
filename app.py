#-*- coding: utf-8 #
"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: LV
"   @module: app
"   @author: Abdullah Alotaibi.
"""

from lv.core.engine.preprocessing import LVPreprocessor
from lv.core.engine.segmentation import LVSegmenter
from lv.core.engine.feature_extraction import LVFeatureExtractor
from lv.core.engine.helpers import LVHelpers as H
from lv.core.engine.recognition import LVRecognizer
from lv.core.api import LVApi

from lv.common.configuration import LVConfig, LVMode, LVFeatureExtractionSettings, LVMLSettings

from lv.ui.api import UIShell
from lv.ui.dispatcher import Dispatcher


def get_img_pool():
  """ get a collection of handwriting testing images."""
  return [
    'lv/test-images/Fancy_CAU.png',
    'lv/test-images/_test_digits_1.jpg',
    'lv/test-images/samp_modi3.jpeg',
    'lv/test-images/samp_full.jpeg',
    'lv/test-images/test_single.jpeg',
    'lv/test-images/csis.jpeg',
    'lv/test-images/photo_1.jpg',
    'lv/test-images/test_cut.jpeg'
    ]

def get_config():
  """ get a sample config object. """
  return LVConfig(LVMode.EN_ALPHABET, LVFeatureExtractionSettings.RASTER_IMAGE , LVMLSettings.MLP)

def no_ui():
  """ playing with engine api directly. """
  config = get_config()
  print config 
  api = LVApi(config, True, 'lv/test-images/Fancy_CAU.png')
  api.scan()
  print(api.output) 
  print api._segmenter.space_anchors

def lv_app():
  """ the entry point for light vision application. """
  # init ui.
  u = UIShell()
  
  # init engine api
  api = LVApi()

  # ui & engine communicate through the dispatcher.
  d = Dispatcher(u, api)

  # enter the message loop.
  u.main_loop()


if __name__ == '__main__':
  """ if executed, trigger components and UI. """

  # no_ui()
  lv_app()
