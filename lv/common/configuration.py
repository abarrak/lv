"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: configuration
"   @author: Abdullah Alotaibi.
"""

from enum import Enum


class LVMode(Enum):
  """ enumeration for the engine languages (modes) used in training and testing. """
  # specify the (MNIST) dataset of handwritten digits.
  DIGITS_ONLY = 1
  # specify the generic (SD19) dataset for alphanumeric handwriting characters.
  EN_ALPHANUMERIC = 2
  # Narrowed (SD19) dataset for alphabet characters only.
  EN_ALPHABET = 3
  # the authors' collected sample (AR16) dataset for alphanumeric arabic characters.
  AR_ALPHABET = 4


class LVFeatureExtractionSettings(Enum):
  """ the available feature extraction choices. """
  # the same pixels grid. No extraction involved.
  RASTER_IMAGE = 1
  # Histogram of Oriented Geometry.
  HOG = 2
  # Moments scalar of a character.
  MOMENTS = 3


class LVMLSettings(Enum):
  """ enumeration for the supported classification methods. """
  KNN = 1
  LOGISTIC_REGRESSION = 2
  SGD = 3

  LINEAR_SVM = 4
  SVM_RBF_KERNEL = 5
  SVM_LIN_KERNEL = 6
  SVM_POLY_KERNEL = 7

  DECISION_TREE = 8
  ADABOOST_DECISION_TREE = 9
  RANDOM_FOREST = 10
  EXTRA_TREES = 11

  MLP = 12
  CNN = 13


class LVConfig(object):
  """ engine configurations to be consumed and communicated with by the LV pipeline components. """
  def __init__(self, mode, fe_settings, ml_settings):
    #TODO: set defaults.
    self.mode = mode
    self.fe = fe_settings
    self.ml = ml_settings

  def __str__(self):
    """ formats a dash-seperated string represntation for the system status. """
    return ("%s_%s_%s" % (str(self.mode).split('.')[1], 
                          str(self.fe).split('.')[1], 
                          str(self.ml).split('.')[1]))
