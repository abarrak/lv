""""
" LightVision, an offline handwriting recognition system.
"
" @package: core
" @module: api    
" @authors: Abdullah Alotaibi.
"""

from lv.core.engine.preprocessing import LVPreprocessor
from lv.core.engine.segmentation import LVSegmenter
from lv.core.engine.feature_extraction import LVFeatureExtractor
from lv.core.engine.configuration import LVConfig, LVMode, LVFeatureExtractionSettings, LVMLSettings
from lv.core.engine.recognition import LVRecognizer
from lv.core.engine.postprocessing import LVPostprocessor
from lv.core.engine.helpers import LVHelpers as H

class LVApi(object):
  """ A high level constructs and facilities of the recognition system. """

  def __init__(self, config=None, is_verbose=False, paper_path=None):

    self._config = config
    self._verbose = is_verbose
    self._paper_path = paper_path

  """ 
  properties and public methods
  """

  @property
  def paper_path(self):
    return self._paper_path

  @paper_path.setter
  def paper_path(self, paper_path):
    self._paper_path = paper_path

  @property
  def preferences(self):
    return self._config
  
  @preferences.setter
  def preferences(self, config):
    self._config = config

  @property
  def verbose(self):
    return self._verbose
  
  @verbose.setter
  def verbose(self, verbose):
    self._verbose = verbose

  @property
  def output(self):
    """ the recognition text output. """
    return self._output

  def scan(self, callback=None):
    """ A wrapping grand abstraction for the pipeline. """
    if not self._config or not self._paper_path:
      raise Exception('LV exception: api requirements missing.')

    # functional style invocation for the engine component.
    # all shared data preserved. 
    # mutual understanding via the engine config.
    self._preprocess()._segment()._feature_extract()._recognize()._post_process()

    print 'size after = %d %d %d' % (self._preprocessor.original.shape)
    
    # get back to asker if a way of communication is presented. return output otherwise.
    if callback:
      callback(self._output)
    else:
      return self._output

  """ 
  private methods
  """

  def _preprocess(self):
    """ start enhancing paper and keep preprocessing instance. """
    self._preprocessor = LVPreprocessor(self._paper_path)
    # .resize((900, 110))
    
    if self._verbose:
      H.show(api._preprocessor.original, 'original image')

      titles = ['grayscale version',
                'gaussian blur smoothing', 
                'adaptive threshold', 
                'thinning filtering',
                'filling and stressing effect']
                
      for t, s in zip(titles, self._preprocessor.detailed_process()):
        H.show(s, t)

    else:
      self._preprocessor.process()

    return self

  def _segment(self):
    """ apply segmentation. """
    self._segmenter = LVSegmenter(self._preprocessor.original, 
                                 self._preprocessor.processed,
                                 self._config)
    self._segmenter.segment()

    if self._verbose:
      self._segmenter.visualize()
      H.show(self._segmenter.original, 'Segmentation Result')

    return self

  def _feature_extract(self):
    """ get the important features and transformation on model data. """
    self._feature_extractor = LVFeatureExtractor(self._segmenter.segments, self._config)
    self._feature_vectors = self._feature_extractor.extract()

    return self

  def _recognize(self):
    """ gateway for the feature learning/predicting tasks. """
    self._recognizer = LVRecognizer(self._config)
    self._recognizer_output = self._recognizer.recognize(self._feature_vectors)

    if self._verbose:
        H.show(self._segmenter.original)

    return self

  def _post_process(self):
    """ combine the result. """
    # give postprocessor the spial and postional data from segmenter, 
    # and recognized sequence of characters from recognizer.
    self._post_processor = LVPostprocessor(self._config, self._segmenter.segments, 
                                           self._segmenter.line_anchors,
                                           self._segmenter.space_anchors,
                                           self._recognizer_output)
    # group and correct.
    self._post_processor.process()
    # final ready text.
    self._output = self._post_processor.output_text

    return self
