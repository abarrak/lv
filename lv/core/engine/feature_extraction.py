"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: feature_extraction.
"   @author: Abdullah Alotaibi.
"""

from skimage import data, color, exposure
from skimage.feature import hog

import numpy as np
import random

from lv.common.configuration import LVFeatureExtractionSettings
from helpers import LVHelpers as H

class LVFeatureExtractor(object):
  """ """

  def __init__(self, x_set, config):
    """
    :param x_set: preprocessd (X), and segmented.
    :param config: instance of LVConfig.
    """
    self.x = x_set
    self._config = config
    self.dimensions = H.datapoints_dimensions(self._config)

    # output storage of extrated features.
    self._feature_vectors = None

  @property
  def feature_vectors(self):
    return self._feature_vectors

  def extract(self):
    """
    :param method: the method to use for feature extraction.
    :return: features vector (numpy array) for the dataset.
    """
    if self._config.fe == LVFeatureExtractionSettings.RASTER_IMAGE:
      self._raster_grid()
    
    elif self._config.fe == LVFeatureExtractionSettings.HOG:
      self._hog()
    
    elif self._config.fe == LVFeatureExtractionSettings.MOMENTS:
      self._moments()
    
    else:
      raise Exception('LV exception: feature extraction method is not found.')

    self._flatten()
    return self.feature_vectors

  def _raster_grid(self):
    """ retain the same feature shape. """
    # Q: should we reshape here or it's the same ?
    self._feature_vectors = self.x

  def _hog(self):
    """
    extract histogram of oriented gradients (HOG) for the data instances.
    :return: fv, array of (HOG) of each features point.
    """
    features = []
    for data in self.x:
      # reshape for image analysis and detecting of HOG.
      data_point = data.reshape(self.dimensions)

      f = hog(data_point, orientations=8, pixels_per_cell=(14, 14),
              cells_per_block=(1, 1), visualise=False)
      features.append(f)

    self._feature_vectors = np.array(features, 'float64')

  def _moments(self):
    pass

  def _flatten(self):
    """ scale data shape in 1-D. """
    for i, fv in enumerate(self._feature_vectors):

      if self.dimensions == (32, 32):
        if self._config.fe == LVFeatureExtractionSettings.RASTER_IMAGE:
          self._feature_vectors[i] = fv.reshape(-1, 1024)
        elif self._config.fe == LVFeatureExtractionSettings.HOG:
          self._feature_vectors[i] = fv.reshape(-1, 36)

      elif self.dimensions == (28, 28):
        
        if self._config.fe == LVFeatureExtractionSettings.RASTER_IMAGE:
          self._feature_vectors[i] = fv.reshape(-1, 784)
        elif self._config.fe == LVFeatureExtractionSettings.HOG:
          self._feature_vectors[i] = fv.reshape(-1, 32)

      else:
        raise Exception('LV exception: Unknown dimensions during feature extraction.')
