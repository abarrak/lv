"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: helpers
"   @author: Abdullah Alotaibi.
"""

import os
import cv2 as cv
import matplotlib.pyplot as plt

from lv.common.configuration import LVMode, LVFeatureExtractionSettings


class LVHelpers(object):

  @staticmethod
  def read(path):
    """ load image for give path in cv object. """
    img = cv.imread(path)
    if img is None:
        raise Exception('Image not found.')
    return img

  @staticmethod
  def show(image, title='sample image'):
    """ helper to reveal the input :image in open-cv viewer. """
    cv.imshow(title, image)
    cv.waitKey(0)
    cv.destroyAllWindows()

  @staticmethod
  def save(image, name):
    """ presists an image to disk. """
    cv.imwrite(name, image)

  @staticmethod
  def current_dir():
    """ fetches the current directory for the running module. """
    return os.curdir

  @staticmethod
  def combine(directory, file):
    print(os.path.join(directory, file))
    return os.path.join(directory, file)

  @staticmethod
  def cobmine_with_current_path(filename):
    """ construct a path of the given filename. Can be foldername too. """
    current = LVHelpers.current_dir()
    path = LVHelpers.combine(current, filename)
    return path

  @staticmethod
  def datasets_path():
    return LVHelpers.cobmine_with_current_path("lv/core/datasets")

  @staticmethod
  def models_path():
    return LVHelpers.cobmine_with_current_path("lv/core/cache/")

  @staticmethod
  def datapoints_dimensions(config):
    """ helper for getting the current dataset size. """
    if config.mode == LVMode.EN_ALPHANUMERIC or config.mode == LVMode.EN_ALPHABET:
      return (32, 32)
    else:
      return (28, 28)

  @staticmethod
  def segmentation_direction(config):
    if config.mode == LVMode.AR_ALPHABET:
      return 'rtl'
    else:
      return 'ltr'
