"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: learning
"   @author: Abdullah Alotaibi.
"""

import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression, Ridge, LogisticRegression, SGDClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier

from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.externals import joblib

from keras.models import Sequential
from keras.models import model_from_json
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.optimizers import SGD, Adam, RMSprop
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

from helpers import LVHelpers as H
from lv.common.configuration import LVMode, LVMLSettings, LVFeatureExtractionSettings


class LVLearning(object):
  """ performs the learning and training phase for the ml model. """

  def __init__(self, config):
    """
    :param LV_config: initialized instance of LVConfig
    """
    self._config = config

    # set learning method as specified in the config. init classifier object.
    self._method = self._config.ml
    self._classifier = None

  """ 
  public interface (API).
  """

  @property
  def model(self):
    """ getter for the model classifier. """
    return self._classifier

  def construct(self, feature_vectors, labels, shapes):
    """ 
    assigns the learning variables and data. Mandatory for new sessions. 
    :param feature_vectors: X of the dataset. scaled to 1-D numpy. splitted to train and test sets.
    :param labels: Y of the dataset (ground truth). splitted to train and test sets.
    :param shapes: shapes of dataset variables (#_samples x #_features)
    """
    self._features_train, self._features_test = feature_vectors
    self._labels_train, self._labels_test = labels
    self._features_shape, self._labels_shape = shapes

  def construct_from_cache(self):
    """ 
    cache models checker and builder.
    :return indicator: (bool) whether the cache build successeded or failed.
    """
    # check if in cache alreay before initializing a new one.
    retrieved = self.retrieve(self._method)
    print retrieved
    if retrieved:
      self._classifier = retrieved
      return True
    else:
      return False

  def train(self):
    """ 
    start the training then cache the model after. 
    :return indicator: (bool) means a successful or failed trainning.
    """
    print("if it's cached, never gonna see this.")
    # set classifier.
    self._classifier = self._get_classifier(self._method)
    
    # fit on x and y.s
    self.fit()

    # cache trained classifer.
    self.persist()

  def fit(self):
    """ factory method for classifier-depandant training. """
    if isinstance(self._classifier, LVNeuralNetworks):
      self._classifier.fit()
    else:
      self._classifier.fit(self._features_train, self._labels_train)

  def predict(self, x_instance):
    """ 
    the magical predictor.
    :param x_instance: an X-compatibale data point.
    :return prediction: (str) represnts the expected character.
    """
    return self._classifier.predict(x_instance)

  def score(self):
    """ test the current model and report accuracy. """
    #TODO: handle cache problem.
    return
    if isinstance(self._classifier, LVNeuralNetworks):
      return self._classifier.score()
    else:
      return self._classifier.score(self._features_test, self._labels_test)

  def persist(self):
    """ save trained model for usage later without training again. """

    if isinstance(self._classifier, LVNeuralNetworks):
      self._classifier.persist(self._get_model_name())
    else:
      modelfile = self._get_model_name() + '.pkl'
      joblib.dump(self._classifier, modelfile, compress=3)

  def retrieve(self, method):
    """ fetch the saved model from disk. """

    modelfile = self._get_model_name()

    try:
      if method == LVMLSettings.CNN or method == LVMLSettings.MLP:
        print('here in nn retr')
        return LVNeuralNetworks(self._config, None).retrieve(modelfile)
      
      else:
        print('here in statistic retr')
        return joblib.load(modelfile + '.pkl')

    except:
      # if file doesn't exists, return none.
      return None

  def report_results(self):
    pass

  def report_confusion_matrix(self):
    pass

  def __str__(self):
    """ :return: (str) a proper textual representation (filename) for the model. """
    return "{}_model".format(str(self._config))

  """ private methods """

  def _get_classifier(self, method):
    """ initialize the classifier object."""
    if method == LVMLSettings.KNN:
      clf = KNeighborsClassifier()

    elif method == LVMLSettings.LOGISTIC_REGRESSION:
      clf = LogisticRegression()

    elif method == LVMLSettings.SGD:
      clf = SGDClassifier(loss='log')

    elif method == LVMLSettings.LINEAR_SVM:
      # linear svm.
      clf = LinearSVC(C=1.0, fit_intercept=True, loss='squared_hinge', max_iter=1000, 
                      multi_class='ovr', penalty='l2', tol=0.0001)

    elif method == LVMLSettings.SVM_RBF_KERNEL:
      # svm with radial basis function 'rbf' as loss kernel.
      clf = svm.SVC(kernel='rbf', gamma='auto', tol=0.01, max_iter=-1, C=1.0)

    elif method == LVMLSettings.SVM_LIN_KERNEL:
      # svm with linear kernel.
      clf = SVC(kernel='linear', gamma='auto', tol=0.01, max_iter=-1, C=1.0)


    elif method == LVMLSettings.SVM_POLY_KERNEL:
      # svm with polynomial kernel.
      clf = SVC(kernel='poly', degree=3),

    elif method == LVMLSettings.DECISION_TREE:
      clf = DecisionTreeClassifier(criterion='gini', max_depth=None, min_samples_split=2, 
                                  splitter='best')

    elif method == LVMLSettings.ADABOOST_DECISION_TREE:
      clf = AdaBoostClassifier(algorithm='SAMME.R', learning_rate=1.5, n_estimators=50,
                              base_estimator=DecisionTreeClassifier(criterion='gini', 
                              max_depth=None, min_samples_split=2, splitter='best'))

    elif method == LVMLSettings.RANDOM_FOREST:
      clf = RandomForestClassifier(bootstrap=True, criterion='gini', n_estimators=10,
                       min_samples_split=2, min_weight_fraction_leaf=0.0)

    elif method == LVMLSettings.EXTRA_TREES:
      clf = ExtraTreesClassifier(bootstrap=False, criterion='gini', n_estimators=10, 
                                min_samples_split=2)

    elif method == LVMLSettings.MLP or method == LVMLSettings.CNN:
      # special claissifer object for nn.
      clf = LVNeuralNetworks(self._config, self._features_shape[1]).construct(
        (self._features_train, self._features_test), (self._labels_train, self._labels_test))

    else:
      raise Exception('LV exception: Unknown classification method.')

    return clf

  def _get_model_name(self):
    """ :return (str) filename for the model presistance. """
    return H.combine(H.models_path(), self.__str__())


class LVNeuralNetworks(object):
  """
  A wrapper for the nerual networks used for model learner. 
  Primary goal to unify api between statistical and ANN estimators models.
  """
  
  def __init__(self, config, input_size):
    """ boots the nn modules. """
    # for reproducibility.
    np.random.seed(1337)
    # configure.
    self._config = config
    self._method = config.ml
    self._input_layer_size = input_size
    self._output_layer_size = self._count_labels()

    # cnn needs the 2d shape of data points. se refactor it here.
    self._img_rows, self._img_cols = H.datapoints_dimensions(self._config)


  def construct(self, feature_vectors, labels):
    """ building a new nn model and assing its paramters. """
    # store data points and sizes. keep them for future usage such as scoring.
    self._features_train, self._features_test = feature_vectors
    
    # labels to 1-hot-vectors
    self._labels_train = np_utils.to_categorical(labels[0], self._output_layer_size)
    self._labels_test = np_utils.to_categorical(labels[1], self._output_layer_size)
    
    # shapes of I/O
    # self._features_shape, self._labels_shape = shapes

    # build model.
    self._setup_model()

    # for chaining.
    return self
    
  def fit(self):
    """ train model on the dataset training portion. """
    # train in batch splitting style.
    batch_size = 128

    # meta-paramter tuning depanding on model.
    if self._method == LVMLSettings.MLP:
      nb_epoch = 30
      verbosity = 2

    elif self._method == LVMLSettings.CNN:
      nb_epoch = 12
      verbosity = 1

    elif self._method == LVMLSettings.RNN:
      raise Exception('Not implemented')
      
    else:
      raise Exception('LV exception: Unknown nn classification method.')

    # cnn has diffenet structure during fitting.
    if self._method == LVMLSettings.CNN:
      self._features_train = self._features_train.reshape(len(self._features_train), 1, 
                                                           self._img_rows,
                                                           self._img_cols)
      self._features_test = self._features_test.reshape(len(self._features_test), 1, 
                                                        self._img_rows, 
                                                        self._img_cols)

    # start fitting.
    self._classifier.fit(self._features_train, self._labels_train, 
                         batch_size=batch_size,
                         nb_epoch=nb_epoch,
                         show_accuracy=True,
                         verbose=verbosity,
                         validation_data=(self._features_test, self._labels_test))

  def score(self):
    """ 
    evaluate the model againts the testing protion of the dataset.
    :return resutl: (list) contains two elements: score and accuracy.
    """
    # get out if training hasn't been done yet.
    if not self._features_test:
      return None

    # take care of cnn.
    if self._method == LVMLSettings.CNN:
      self._features_test = self._features_test.reshape(len(self._features_test), 1, 
                                                        self._img_rows, 
                                                        self._img_cols)

    return self._classifier.evaluate(self._features_test, self._labels_test,
                                     show_accuracy=True, verbose=0)

  def predict(self, x_instance):
    """ 
    get an estimation for a new observation x.
    :return (str) a predicated label.
    """
    # cnn has diffenet structure for input point.
    if self._method == LVMLSettings.CNN:
      x_instance = x_instance.reshape(1, 1, self._img_rows, self._img_cols)

    return self._classifier.predict_classes(x_instance, batch_size=1)

  def persist(self, model_name):
    """ fetch the saved model from disk. """
    json_string = self._classifier.to_json()

    open(model_name + '_architecture.json', 'w').write(json_string)
    self._classifier.save_weights(model_name + '_weights.h5')

  def retrieve(self, model_name):
    """ save trained model for usage later without training again. """
    model = model_from_json(open(model_name + '_architecture.json').read())
    model.load_weights(model_name + '_weights.h5')
    print('internal nn retr', model)

    self._classifier = model
    return self

  """ 
  private methods
  """

  def _setup_model(self):
    """ factory for the nueral nets classifier. """
    if self._method == LVMLSettings.MLP:
      self._classifier = self._setup_mlp()

    elif self._method == LVMLSettings.CNN:
      self._classifier = self._setup_cnn()

    elif self._method == LVMLSettings.CNN:
      self._classifier = self._setup_rnn()

    else:
      raise Exception('LV exception: Unknown nn classification method')

  def _setup_mlp(self):
    """ construct and return a mlp neural net."""
    # gather hayper-parameters.
    inputlayer_shape = (self._input_layer_size,)
    outputlayer_size = self._output_layer_size

    model = Sequential()

    # different setup for each feature extraction method.
    if self._config.fe == LVFeatureExtractionSettings.RASTER_IMAGE:
      # first layer.
      model.add(Dense(512, input_shape=inputlayer_shape))
      model.add(Activation('relu'))
      model.add(Dropout(0.2))

      # second hidden layer.
      model.add(Dense(512))
      model.add(Activation('relu'))
      model.add(Dropout(0.2))
      
      # output layer.
      model.add(Dense(outputlayer_size))
      model.add(Activation('softmax'))

    elif self._config.fe == LVFeatureExtractionSettings.HOG:
      # first layer. 330 neurons best for Hog !
      model.add(Dense(330, input_shape=inputlayer_shape))  
      model.add(Activation('relu'))
      model.add(Dropout(0.2))

      # second hidden layer.
      model.add(Dense(330))
      model.add(Activation('relu'))
      model.add(Dropout(0.2))
      
      # output layer.
      model.add(Dense(outputlayer_size))
      model.add(Activation('softmax'))

    elif self._config.fe == LVFeatureExtractionSettings.MOMENTS:
      raise Exception('LV exception: Not implemented.')

    else:
      raise Exception('LV exception: Unknown feature extraction method.')

    rms = RMSprop()
    model.compile(loss='categorical_crossentropy', optimizer=rms)

    return model

  def _setup_cnn(self):
    """ """
    output_size = self._output_layer_size
    # number of convolutional filters to use
    nb_filters = 32
    # size of pooling area for max pooling
    nb_pool = 2
    # convolution kernel size
    nb_conv = 3

    # different setup for each feature extraction method.
    if self._config.fe == LVFeatureExtractionSettings.RASTER_IMAGE:

      model = Sequential()

      model.add(Convolution2D(nb_filters, nb_conv, nb_conv,
                              border_mode='valid',
                              input_shape=(1, self._img_rows, self._img_cols)))
      model.add(Activation('relu'))
      model.add(Convolution2D(nb_filters, nb_conv, nb_conv))
      model.add(Activation('relu'))
      model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))
      # model.add(Dropout(0.25))
      model.add(Dropout(0.2))

      model.add(Flatten())
      # model.add(Dense(128)) mnist standard neurons number.
      model.add(Dense(512))
      model.add(Activation('relu'))
      # model.add(Dropout(0.5))
      model.add(Dropout(0.4))
      model.add(Dense(output_size))
      model.add(Activation('softmax'))

    elif self._config.fe == LVFeatureExtractionSettings.HOG:
      raise Exception('LV exception: CNN with Hog Not implemented.')

    elif self._config.fe == LVFeatureExtractionSettings.MOMENTS:
      raise Exception('LV exception: Not implemented.')

    else:
      raise Exception('LV exception: Unknown feature extraction method.')

    model.compile(loss='categorical_crossentropy', optimizer='adadelta')
    return model

  def _setup_rnn(self):
    raise Exception('LV exception: not implemented')


  def _count_labels(self):
    """ how much distinct class for each dataset. """
    if self._config.mode == LVMode.DIGITS_ONLY:
      return 10
    elif self._config.mode == LVMode.EN_ALPHANUMERIC:
      return 62
    elif self._config.mode == LVMode.EN_ALPHABET:
      return 52
    elif self._config.mode == LVMode.AR_ALPHABET:
      pass 
    else:
      raise Exception('LV exception: mode settings error.')
