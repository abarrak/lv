"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: loader.
"   @author: Abdullah Alotaibi.
"""

import csv
import os
import numpy as np
import pandas as pd
from sklearn import datasets
from helpers import LVHelpers as H

class LVLoader(object):
  """ 
  Acquires the datasets for the model learning. 
  Applies the needed clean-up and transformation operations. 
  """

  @staticmethod
  def load_mnist():
    # load the dataset.
    folder = H.datasets_path()
    dataset = datasets.fetch_mldata("MNIST Original", data_home=folder)

    # store observations (X) and targets (Y) in vectorized efficient numpy arrays.
    observations = np.array(dataset.data, 'int16')
    targets = np.array(dataset.target, 'int')    

    return [observations, targets]

  @staticmethod
  def load_sd19():
    # get sd19 raw frame.
    df = LVLoader._get_sd19()

    # use ascii for labels for more efficient numpy.
    symbol_map = LVLoader.get_english_alphanumeric_map()

    # observations are normalized to 1024 point. targets are one point indicating the character.
    observations = df.ix[:, 0:1023].as_matrix()
    targets = df.ix[:,1024].replace(symbol_map).as_matrix()

    return [observations, targets]

  @staticmethod
  def load_strict_sd19():
    # get sd19 raw frame.
    df = LVLoader._get_sd19()

    # projects alphabet only.
    df = df[df[1024] > '9']
    
    # use ascii for labels for more efficient numpy.
    symbol_map = LVLoader.get_english_alphabet_map()

    # observations are normalized to 1024 point. targets are one point indicating the character.
    observations = df.ix[:, 0:1023].as_matrix()
    targets = df.ix[:,1024].replace(symbol_map).as_matrix()

    return [observations, targets]

  @staticmethod
  def get_english_alphanumeric_map():
    """ 
    conducts the adjustments to unify the data units in the dataset.
    :return symbol_map: (dict) contains character id and value.
    """
    char_only_ascii = [chr(x) for x in range(48, 58) + range (65, 91) + range(97, 123)]
    numeric_classes = [c for c in range(0, 62)]

    return dict(zip(char_only_ascii, numeric_classes))

  @staticmethod
  def get_english_alphabet_map():
    char_only_ascii = [chr(x) for x in range (65, 91) + range(97, 123)]
    numeric_classes = [c for c in range(0, 52)]

    return dict(zip(char_only_ascii, numeric_classes))

  @staticmethod
  def get_arabic_alphabet_map():
    pass


  @staticmethod
  def scale_for_dataset(x_set):
    """ 
    scaling a given dataset to [0, 1] only.
    Thanks to numpy. we can deal in batches or instances.
    """
    # mnist is a special case. Needs some scaling for input observations.
    return np.multiply(x_set.astype(np.float32), 1.0 / 255.0)

  """ private methods. """
  
  @staticmethod
  def _get_sd19():
    """ obtain dataset in prepared frame. """
    # locate path.
    path = H.datasets_path() + '/sd19.csv';
    # load data set into pandas frame and return it.
    df = pd.read_csv(path, header=None)
    return df
