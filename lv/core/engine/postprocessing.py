"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: postprocessing
"   @author: Abdullah Alotaibi.
"""


class LVPostprocessor(object):

  def __init__(self, config, segments, line_anchors, space_anchors, recognition_sequence):
    # define fields.
    self._config = config
    self._segments = segments
    self._space_anchors = space_anchors
    self._line_anchors = line_anchors
    self._recognition_sequence = recognition_sequence
    # postprocessor output.
    self._output_text = ""

  @property
  def output_text(self):
    """ the final postprocessed text. """
    return self._output_text

  def process(self):
    # apply grouping
    self._group()

    # apply spell check.
    self._spell_check()

  def _group(self):
    """ organize lines and words of the text. """
    g_text = ""

    for i in range(len(self._recognition_sequence)):
      # add char.
      g_text += self._recognition_sequence[i]
      
      # if it's place of new line, add one.
      if i in self._line_anchors:
        g_text += "\n"

      # if it's place of a space, add one.
      if i in self._space_anchors:
        g_text += " "

    self._output_text = g_text
    
  def _spell_check(self):
    # stup for now ..
    pass