"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: preprocessing.
"   @author: Abdullah Alotaibi.
"""

import cv2 as cv
import numpy as np


class LVPreprocessor(object):
  """ Holds the image processing operations for the recognition engine pipeline. """

  def __init__(self, image_path):
    """
    :param image_path: (str) the image path.
    """
    # read image file
    self._original = self.load(image_path)
    # save copy for working.
    self._processed = self._original.copy()

    # 900 as un upperbound 
    self.scaled_width_resize(900)

  def load(self, path):
    """ obtain the image file from the specified :path. """
    img = cv.imread(path)

    if img is None:
        raise Exception('LV exception: Image not found.')
    return img

  def resize(self, dimensions):
    """ reshape original images as specified in :dimension tuple. """
    self._original = self._processed = cv.resize(self._processed, dimensions,
                                                interpolation=cv.INTER_AREA)
    return self

  def scaled_width_resize(self, new_width):
    """ reshape while preserving aspect ratio. """
    # obtain image height & width before resizing.
    h, w, channels = self._original.shape

    if w > new_width:
      # aspect ration = original width / original height.
      aspect_ratio =  w / h
      # adjusted height.
      new_height = new_width // aspect_ratio
      # inter_area is a paramter for resizing algorithm.
      self._original = self._processed = cv.resize(self._processed, (new_width, new_height),
                                                interpolation=cv.INTER_AREA)


  @property
  def original(self):
    return self._original

  @property
  def processed(self):
    return self._processed

  def process(self):
    """
    helper to apply all preprocessing at once with the defaults params.
    """
    self.prepare()
    self.smooth()
    self.threshold()
    self.thin()
    self.stress()

  def detailed_process(self):
    """
    helper to apply all preprocessing at once with the defaults params.
    yields processing result after each step.
    """
    self.prepare()
    yield self._processed
    self.smooth()
    yield self._processed
    self.threshold()
    yield self._processed
    self.thin()
    yield self._processed
    self.stress()
    yield self._processed

  def prepare(self):
    """ convert the image to greyscale. """
    self._processed = cv.cvtColor(self._processed, cv.COLOR_BGR2GRAY)

  def threshold(self, kind='adaptive', fixed_threshold=130, adaptive_type='mean',
      adaptive_cell_size=35, adaptive_c_param=17):
    """
    :param kind: 'fixed' or 'adaptive'.
    :param fixed_threshold: the threshold constant in 'fixed' case.
    :param adaptive_type: 'mean' or 'gaussian'.
    :param adaptive_cell_size: n for the region size (n x n).
    :param adaptive_c_param: substraction constant.
    :return: a binary version of the input image.
    """
    if kind == 'fixed':
      ret, thresholded = cv.threshold(self._processed, fixed_threshold, 255, cv.THRESH_BINARY_INV)

    elif kind == 'adaptive':
      if adaptive_type == 'mean':
          method = cv.ADAPTIVE_THRESH_MEAN_C
      elif adaptive_type == 'gaussian':
          method = cv.ADAPTIVE_THRESH_GAUSSIAN_C
      else:
          raise Exception('Unknown adaptive threshod method.')

      thresholded = cv.adaptiveThreshold(self._processed, 255, method,
                                          cv.THRESH_BINARY_INV,
                                          adaptive_cell_size,
                                          adaptive_c_param)
    else:
      raise Exception('Unknown threshold method.')

    self._processed = thresholded

  def smooth(self, method='gaussian'):
    """ blur filtering to remove noise in pixels. """
    if method == 'blur':
        self._processed = cv.blur(self._processed, (5, 5))
    elif method =='gaussian':
        self._processed = cv.GaussianBlur(self._processed, (5, 5), 0)
    else:
        raise Exception('Unknown smoothing method.')

  def thin(self, kernel=(2, 2)):
    """ reduce the line stroke of the shape. """
    k = np.ones(kernel, np.uint8)
    self._processed = cv.erode(self._processed, k, iterations=1)

  def stress(self, kernel=(1, 1)):
    """ increase the thickness of the shape. """
    k = np.ones(kernel, np.uint8)
    self._processed = cv.dilate(self._processed, k)

  def remove_noise(self, kernel):
    """ removes noisy pixels in the area. """
    self._processed = cv.morphologyEx(self._processed, cv.MORPH_OPEN, kernel)

  def fill(self, kernel):
    """ fill in gaps in the shapes structure. """
    self._processed = cv.morphologyEx(self._processed, cv.MORPH_CLOSE, kernel)
