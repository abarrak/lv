"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: recognition
"   @author: Abdullah Alotaibi.
"""

from __future__ import division
import numpy as np

from sklearn.cross_validation import train_test_split

from lv.common.configuration import LVMode, LVFeatureExtractionSettings
from loader import LVLoader
from feature_extraction import LVFeatureExtractor
from learning import LVLearning
from helpers import LVHelpers as H

class LVRecognizer(object):

  def __init__(self, config):

    # model settings.
    self._config = config

    # learner input variables.
    self._x_train = None
    self._y_train = None
    self._x_test = None
    self._y_test = None

    # start the wizard
    self._initalize_learner()

  @property
  def model(self):
    """ accessor for the learning model. """
    return self._model

  def recognize(self, feature_vectors):
    """
    a wrapper for the ml model predictor.
    :param feature_vectors: one or more unseen data points to be recognized
    :return texts: list of (str) represents the predicted characters.
    """
    texts = []
    #TODO: turn it into an iterator for better performance.
    for fv in feature_vectors:
      # mnist is a special case. needs scaling.
      if self._config.mode == LVMode.DIGITS_ONLY and self._config.fe != LVFeatureExtractionSettings.HOG:
        fv = LVLoader.scale_for_dataset(fv)

      prediction = self._model.predict(fv)
      l = self._convert_prediction_to_label(prediction)

      if isinstance(l, np.ndarray):
        l = str(l[0])

      texts.append(l)

    return ''.join(texts)


  """ 
  private methods. 
  """

  def _initalize_learner(self, test_split_size=0.2):
    """ prepare and init the learning process. """
    # init the learner object
    learner = LVLearning(self._config)

    # first, ask learner if she can get her hand on a cached model.
    if learner.construct_from_cache():
      self._model = learner
      ## TODO:
      ## need to architect a mechanism for keepin training session scores and results.

    # if not, start a learning session.
    else:
      # fetch required dataset.
      self._x_all, self._y_all = self._get_dataset()

      # scale if mnist.
      if self._config.mode == LVMode.DIGITS_ONLY and self._config.fe != LVFeatureExtractionSettings.HOG:
        self._x_all = LVLoader.scale_for_dataset(self._x_all)

      # apply transformations on dataset's feature space.
      self._extract_features()

      # split data into 90% training and 10% testing.
      splits = train_test_split(self._x_all, self._y_all, test_size=test_split_size)
      self._x_train, self._x_test, self._y_train, self._y_test = splits

      # assign the shape of the instances in X and Y.
      self._x_shape = self._x_train.shape
      self._y_shape = self._y_train.shape

      # construct the learner and give her features, labels, shapes and a copy of the config.
      fv = (self._x_train, self._x_test)
      lb = (self._y_train, self._y_test)
      sh = (self._x_shape, self._y_shape)

      learner.construct(fv, lb, sh)
      self._model = learner

      # begin the training session.
      self._model.train()

  def _get_dataset(self):
    """ fetch dataset as specified in config file. """

    if self._config.mode == LVMode.DIGITS_ONLY:
      return LVLoader.load_mnist()
    elif self._config.mode == LVMode.EN_ALPHANUMERIC:
      return LVLoader.load_sd19()
    elif self._config.mode == LVMode.EN_ALPHABET:
      return LVLoader.load_strict_sd19()
    elif self._config.mode == LVMode.AR_ALPHABET:
      pass
    else:
      raise Exception('LV exception: mode settings error.')

  def _extract_features(self):
    """ do necessary transformation on datapoints in the dataset before learning. """
    self._x_all = LVFeatureExtractor(self._x_all, self._config).extract()

  def _convert_prediction_to_label(self, prediction):
    """ get back from the numerical scaled classing for model learning to the real world. """
    # inverse lookup for the labels map to get actual character.
    lookup = lambda cmap, pred: (k for k, v in cmap.items() if v == pred).next()

    if self._config.mode == LVMode.EN_ALPHANUMERIC:
      return lookup(LVLoader.get_english_alphanumeric_map(), prediction)

    elif self._config.mode == LVMode.EN_ALPHABET:
      return lookup(LVLoader.get_english_alphabet_map(), prediction)

    elif self._config.mode == LVMode.AR_ALPHABET:
      return lookup(LVLoader.get_arabic_alphabet_map(), prediction)

    else:
      # in mnist, they're the same.
      return prediction
