"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: core.engine
"   @module: segmentation
"   @author: Abdullah Alotaibi.
"""

import cv2
import numpy as np

from helpers import LVHelpers as H


class LVSegmenter(object):
  """ Contains the locating and segmenting logic for the engine. """

  def __init__(self, original, preprocessed, config):
    """
    :param original: unprocessed version of the image to draw segments on if wanted.
    :param preprocessed: ready image to get text from, in chunks sequence 'segments'
                         for its characters.
    :param config: an instance of LVConfig.
    """
    # keep a special copy of images. (serious step. most ops are destructive).
    self._original = original.copy()
    self._segmented = preprocessed.copy()
    self._config = config

    # segmentation information storage.
    self._raw_segments = []
    self._normalized_segments = []
    self._line_anchors = []
    self._space_anchors = []

  """ 
  public methods and helpers.
  """

  @property
  def original(self):
    """ original (untouched) image holder. """
    return self._original

  @property
  def segmented(self):
    """ segmented (working) image holder. """
    return self._segmented

  @property
  def contours(self):
    """ extracted contours objects in the image. """
    return self._contours

  @property
  def hierarchy(self):
    """ hierarchy lists for contours as retured from the extraction procedure. """
    return self._hierarchy

  @property
  def segments(self):
    """ normalized segments extracted from the contours objects. """
    return self._normalized_segments
  
  @property
  def line_anchors(self):
    """ after which segments there is a new line ?. 0-based indices. """
    return self._line_anchors

  @property
  def space_anchors(self):
    """ after which segments there is a space ?. 0-based indices. """
    return self._space_anchors
  
  def segment(self):
    """
    run the structural analysis and segmentation operations.
    :return: a list of segmented normalized images.
    """
    self._get_bounding_boxes()
    self._sort('ltr')

    for box in self._bounding_boxes:

      segment = self._fragment(box)

      # if bad segmenting .. pass.
      if not segment.any():
          continue
      # to see segments during debug.
      H.show(segment, 'seg')

      self._raw_segments.append(segment)

      # normalize.
      n = self._normalize(segment)
      self._normalized_segments.append(n)

    # seperate lines and words.
    self._segment_words()
    self._segment_lines()

    return self

  def visualize(self, stroke=1):
    """ show rectangles around the text bounding boxes in the image. """
    for bbox in self._bounding_boxes:
      x = bbox[0]
      y = bbox[1]
      w = bbox[2]
      h = bbox[3]

      p1 = (x, y)
      p2 = (x+w, y+h)

      cv2.rectangle(self._original,
                  p1, p2,
                  (0, 255, 0), stroke)
    return self

  def draw(self, stroke=2):
    """ paint the contours shapes. """
    cv2.drawContours(self._original, self._contours, -1, (0, 0, 255), stroke)


  """
  private methods
  """

  def _get_bounding_boxes(self):
    """ process text regions as boxes. """
    permit_area = 18

    # obtain the contours (ndarray) and their hierarchy .. # RETR_TREE.
    self._contours, self._hierarchy = cv2.findContours(self._segmented.copy(),
                                                       cv2.RETR_EXTERNAL, 
                                                       cv2.CHAIN_APPROX_SIMPLE)

    # set bounding box for each contour and return them if their size permits..
    self._bounding_boxes = [cv2.boundingRect(c) for c in self._contours
                            if cv2.contourArea(c) > permit_area]

    return self._bounding_boxes

  def _fragment(self, bbox):
    """ sets a rectangular regain of the text bounding box. """
    x = bbox[0]
    y = bbox[1]
    w = bbox[2]
    h = bbox[3]

    ## old implementation .. Still gives goodies do not delete.
    length = int(h * 1.6)
    pt1 = int(y + h // 2 - length // 2)
    pt2 = int(x + w // 2 - length // 2)
    region = self._segmented[pt1:pt1+length, pt2:pt2+length]

    # region = self._segmented[y:y+h, x:x+w] # bad impel.
    print "x = %d , y = %d " % (x, y)
    return region

  def _normalize(self, segment):
    """ resize the segment to adhere for the model dataset instances. """
    # fetch dataset instances shape.
    dimensions = H.datapoints_dimensions(self._config)
    # wrap resize in try/catch just in case.
    try:
      if len(segment) != 0:
          return cv2.resize(segment, dimensions, interpolation=cv2.INTER_AREA)
    except:
      raise Exception("Too small segment encounterd. Normalization failed.")

  def _sort(self, direction="ltr"):
    """
    internal sorter method for the contours in the image.
    :param direction: one of (4) options: 'ltr', 'rtl', 'from-top', 'from-bottom'.
    """
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if direction == "rtl" or direction == "from-bottom":
      print 'reversed ...............'
      reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if direction == "from-top" or direction == "from-bottom":
      print 'horizon .............'
      i = 1

    # sort contours and bounding boxes as provided criteria.
    shapes = zip(self._contours, self._bounding_boxes)
    sorted_shpes = sorted(shapes, key=lambda b:b[1][i], reverse=reverse)
    # sorted_shpes = sorted(shapes, key=lambda b:b[1][0] + b[1][1])
    # sorted_shpes = sorted(sorted_shpes, key=lambda b:b[1][1])
    
    self._contours, self._bounding_boxes = zip(*sorted_shpes)

  
  def _specify_epslion(self, criteria, technique='constant'):
    """
    defines the epsilon value for lines splitting.
    :param criteria: 'lines' or 'words'.
    :param technique: 'constant' or 'divers'
    """
    # set criteria.
    if criteria == 'lines':
      axis_len = self._original.shape[0]
    elif criteria == 'words':
      axis_len = self._original.shape[1]
    else:
      raise Exception('LV exception: unknown criteria for epsilon.')

    # set esp depending on technique.
    eps = None
    if technique == 'constant':
      eps = 99
    elif technique == 'divers':
      # branch with respect to criteria (width/height)
      for n in xrange(100, 1100, 100):
        if n > axis_len > n - 99:
          eps = n - 1 / 10
    else:
      raise Exception('LV exception: unknown criteria for epsilon.')      

    return eps

  def _segment_lines(self):
    """ implementation for epsilon based line splitting algorithm. """
    # get epsilon for lines.
    e = self._specify_epslion('lines')

    # loop over segemts and examine every adjucent cells by e
    for i in xrange(len(self._bounding_boxes) - 1):

      # obtain two neighbors segments
      first, second = self._bounding_boxes[i], self._bounding_boxes[i + 1]

      # compare y_axis positions.
      if (second[1] - first[1]) > e:
        # add new line anchor after i
        self._line_anchors.append(i)

  def _segment_words(self):
    """ implementation for epsilon based word splitting algorithm. """
    # get epsilon for words.
    e = self._specify_epslion('words')

    # loop over segemts and examine every adjucent cells by e
    for i in xrange(len(self._bounding_boxes) - 1):

      # obtain two neighbors segments
      first, second = self._bounding_boxes[i], self._bounding_boxes[i + 1]

      # compare x_axis positions.
      if (second[0] - first[0]) > e:
        # add new line anchor after i
        self._space_anchors.append(i)
