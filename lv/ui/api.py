#-*- coding: utf-8 #
"""
"   LightVision, a handwriting recognition system.
"
"   @package: ui
"   @module: api
"   @summary: contains the application UI abstract level constructs.
"   @author: Abdullah Alotaibi.
"""

import sys
from PySide import QtCore, QtGui
import core as ui

class UIShell(object):
  """ the high level UI layer exposed to other app components. """

  def __init__(self):
    # initialize a Qt skeleton UI app
    self.app = QtGui.QApplication(sys.argv)
        
    # start the main window
    self._window = ui.UIWindow()

    # a wrapper attribute for UIHelper
    self._helpers = ui.UIHelpers(self._window)

  def main_loop(self):
    """ gets called at the end of the program to enter the app event loop. """
    sys.exit(self.app.exec_())

    """ UI API: public properties and methods. """

  @property
  def helpers(self):
    """ 
    wraps UI helper class. the methods can be invoke on helpers are:
    askUser() , popup() , error()
    [param]: for all => title (str),  text (str).
    [return]: only askUser returns a bool for user choice.
    """
    return self._helpers

  def scanned(self, listener):
    """
    :param listener: function to be notified when scanned event occurs.
    """
    # hooks up a listeners to the scanned event.
    self._window.notify(listener)

  @property
  def user_image_path(self):
    """ :return: (str), the user image input. """
    return self._window.document_path

  @property
  def user_mode(self):
    return self._window.lv_mode.currentText()

  @property
  def user_fe(self):
    return self._window.lv_fe.currentText()

  @property
  def user_ml(self):
    return self._window.lv_ml.currentText()

  @property
  def is_verbose_output(self):
    """ :return: (str), the user image input. """
    if self._window.verbose.checkState() == QtCore.Qt.Checked:
      return True
    else:
      return False

  def recognition_output(self, outtput_text):
    """
    set recognition output of scanned image to the review editor
    :param (str):
    """
    self._window.review_editor.setText(str(outtput_text))

  def classification_heuristics(self, details=[]):
    """ :param heuristics: a list of [classification report, classification matrix]. """
    self._window.heuristics = heuristics
