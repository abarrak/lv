#-*- coding: utf-8 #
"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: ui
"   @module: core
"   @summary: contains the application UI functionality and units.
"   @author: Abdullah Alotaibi.
"""

from PySide import QtCore, QtGui
from utils import APP_C, Util

class UIWindow(QtGui.QMainWindow):
    """ :summary: the main component of UI layer class. """

    def __init__(self):
        
        # initialize windows widget.
        super(UIWindow, self).__init__()

        # initialize components,
        self._bootstrap()

    '''
    public interface
    '''

    def notify(self, listener):
        """ :summary: register listeners for UI main event 'scanned' to be notified. """
        self._scannedListener.append(listener)

    @property
    def verbose(self):
        return self._verbose

    @property
    def lv_mode(self):
        return self._lv_mode

    @property
    def lv_fe(self):
        return self._lv_fe

    @property
    def lv_ml(self):
        return self._lv_ml

    @property
    def verbose(self):
        return self._verbose

    '''
    private (internal) methods ..
    '''

    def _bootstrap(self):
        """ start the construction procedures for window. """
        self._configure()
        self._render()

    def _configure(self):
        """ generic configuration. """
        # configure common UI settings 
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))

        # initiate the abstract scan event, hook its handler, 
        # and initialize its listener collection
        self._scanned = UIScanningEvent()
        self._scanned.scanningSignal.connect(self._onScanned)
        self._scannedListener = []

    def _render(self):
        """ correlate rendering methods. """
        self._renderWindow()
        self._prepareLayout()
        self._renderMenubar()
        self._defineMembers()
        self._renderUIComponents()
        self._concealLayout()

        # show the main ui component 'window' after rendering completes.
        self.show()

    def _renderWindow(self):
        """ setup main window structure. """
        self.setWindowTitle(APP_C['config.app_title'])
        self.setWindowIcon(QtGui.QIcon(APP_C['config.icon_path']))
        self.setFont(QtGui.QFont('Arial', 12))

        self.setFixedSize(900, 650)
        self.setWindowFlags(QtCore.Qt.WindowMinimizeButtonHint)     
        self._centerWindow()

    def _centerWindow(self):
        """ gets and fix coordinates for centering property. """
        window_rect = self.frameGeometry()
        center_point = QtGui.QDesktopWidget().availableGeometry().center()
        window_rect.moveCenter(center_point)
        
        self.move(window_rect.topLeft())

    def _prepareLayout(self):
        # instantiate private property for the layout
        self._grid = QtGui.QGridLayout()
        # self._grid.setSpacing(5)

    def _concealLayout(self):
        # create a widget to contain our layout 
        # and set it central to window
        wid = QtGui.QWidget()
        
        wid.setLayout(self._grid)
        
        self.setCentralWidget(wid)

    def _renderMenubar(self):
        # render menus actions
        actions = self._renderMenusActions()

        # init a menu-bar and puplates munes with actions ..
        menubar = self.menuBar()

        mainMenu = menubar.addMenu('&Main')
        helpMenu = menubar.addMenu('&Help')
        
        for a in actions[:3]:
            mainMenu.addAction(a)
        mainMenu.addSeparator()
        mainMenu.addAction(actions[3])

        for a in actions[4:]:
            helpMenu.addAction(a)

    def _renderMenusActions(self):
        # set up menus' actions.
        tempMsgbox = UIHelpers(self)
        
        bp = [
            ('Browse', 'Ctrl+O', self._onBrowseBtnClicked),
            ('Scan', 'Enter', self._onScanBtnClicked),
            ('Export', 'Ctrl+E', self._onExportBtnClicked),
            ('Clear', 'Backspace', self._onClearBtnClicked),
            ('Exit', 'Esc', self.close),
            ('Help', 'F1', lambda: tempMsgbox.popup('Help', APP_C['text.help'])),
            ('About', 'F3', lambda: tempMsgbox.popup('About', APP_C['text.about']))
        ]

        return [self._drawMenuAction(*i) for i in bp]

    def _drawMenuAction(self, name, shortcut, e_handler):
        # create and return action as specified in arguments.
        action = QtGui.QAction(('&%s' % name), self)
        action.setShortcut(shortcut)
        action.triggered.connect(e_handler)

        return action

    def _defineMembers(self):
        """ for clarity, initialize the varoious state holders. """
        # the path for user sumbitted img.
        self.document_viewer = None
        # language mode.
        self._lv_mode = None
        # machine learning method.
        self._lv_ml = None
        # feature extraction method.
        self._lv_fe = None
        # review editor widget.
        self.review_editor = None
        # verbose checkbox widget.
        self._verbose = None
        # filename label control
        self.document_filename = None
        # filepath from file picker.
        self.document_path = None

    def _renderUIComponents(self):
        """ set UI main parts and controls. """
        self._renderLogo()
        self._renderSettingsControls()
        self._renderEditorialArea()
        self._renderButtons()

    def _renderLogo(self):
        """ create and reveal the logo. """
        logo = QtGui.QPixmap(APP_C['config.logo_path'])
        logo_label = QtGui.QLabel('Light Vision Application')
        logo_label.setPixmap(logo)

        self._grid.addWidget(logo_label, 0, 0, 1, 3)

    def _renderSettingsControls(self):
        """ render the two rows of controls after logo row. """
        self._renderListBoxes()
        self._renderBrowse()
        self._renderVerbose()

    def _renderListBoxes(self):
        """ render the dashboard lists. Activate them for user customization. """
        # populate.
        labels = 'Language Mode: ', 'ML Method:', 'Feature Extraction:'
        modes, fe, ml = Util.strigify_lv_config()
        
        list_boxes = []
        for l, o, col in zip(labels, [modes, ml, fe], range(3)):
            list_boxes.append(self._renderListBox(l, o, (1, col * 3, 1, 3)))

        # monitor the user choices controls.
        self._lv_mode, self._lv_ml, self._lv_fe = list_boxes

    def _renderListBox(self, label, option_list, coords):
        """
        takes a label and option list and constuct a bundled list widget.
        :return list_box: the widget, to keep track of it.
        """
        list_label = QtGui.QLabel(label)
        list_box = QtGui.QComboBox()
        list_box.setEditable(False)

        for option in option_list:
            list_box.addItem(option)

        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(list_label)
        hbox.addWidget(list_box)

        r, c, rs, cs = coords
        self._grid.addLayout(hbox, r, c, rs, cs, QtCore.Qt.AlignmentFlag.AlignHCenter)

        return list_box

    def _renderBrowse(self):
        """ add browse and filename display widgets. """
        self._renderButton('Browse ...', 2, 0, self._onBrowseBtnClicked)

        file_name = QtGui.QLabel('...')
        self._grid.addWidget(file_name, 2, 1, 1, 3, QtCore.Qt.AlignmentFlag.AlignLeft)

        # store a reference for filename.
        self.document_filename = file_name

    def _renderVerbose(self): 
        """ setting verbose checkbox control and keep track of it. """
        # create.
        self._verbose = QtGui.QCheckBox('Show detailed ouput.', self)
        # add to layout.
        self._grid.addWidget(self._verbose, 2, 6, 1, 2)

    def _renderEditorialArea(self):
        """ scafold the document viewer and review editor widgets needed arrangemet. """

        # arrange according to vetical box layout.
        labels = 'Document Image', 'Review Editor'
        widgets = self._getViewer(), QtGui.QTextEdit()

        vboxes = []
        for l, w in zip(labels, widgets):
            vbox = QtGui.QVBoxLayout()
            vbox.addWidget(QtGui.QLabel(l))
            vbox.addWidget(w)
            vboxes.append(vbox)

        # hbox = QtGui.QHBoxLayout()
        # hbox.addLayout(vboxes[0])
        # hbox.addSpacing(10)
        # hbox.addLayout(vboxes[1])

        self._grid.addLayout(vboxes[0], 4, 0, 4, 4)
        self._grid.addLayout(vboxes[1], 4, 4, 4, 5)

        # keep track of thoes two controls.
        self.document_viewer, self.review_editor = widgets

    def _getViewer(self):
        """ build image viewer. """
        viewer = QtGui.QLabel()
        viewer.setBackgroundRole(QtGui.QPalette.Base)
        viewer.setScaledContents(True)

        return viewer

    def _renderButtons(self):
        """ draw buttons and wire their interaction logic. """
        self._renderButton('Clear', 8, 0, self._onClearBtnClicked)
        self._renderButton('Scan', 8, 7, self._onScanBtnClicked, size=200)
        self._renderButton('Export', 8, 8, self._onExportBtnClicked, size=200)

    def _renderButton(self, text, row, cloumn, e_handler=None, rowspan=1, colspan=1, size=80):
        """ helper that create flexiable buttons """
        btn = QtGui.QPushButton(text)
        btn.setMaximumWidth(btn.fontMetrics().boundingRect(text).width() + size)
        
        btn.clicked.connect(e_handler or self._onDummyBtnClicked)

        self._grid.addWidget(btn, row, cloumn, rowspan, colspan)
    
    ''' 
    event handlers
    '''

    def _onScanned(self):
        """ catcher of scan click event. she alert the registerers. :) """
        if not self.document_path:
            return

        # notify the registered listeners when the event is raised
        for l in self._scannedListener:
            l()

    def _onBrowseBtnClicked(self):
        # fire up the file picker and log filename.
        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file')

        if fname:
            if Util.is_allowed_file(fname):
                # show the name on lable.
                self.document_filename.setText(fname.split('/')[-1])
                # save the path.
                self.document_path = fname
                # show the picture.
                self.document_viewer.setPixmap(fname)
            else:
                UIHelpers(self).error(text="please choose an image format.")


    def _onClearBtnClicked(self):
        """ refresh app and start over. """
        # clear editor and get rid of viewer image.
        self.review_editor.clear()
        # clear document name, path, and image.
        self.document_filename.setText('')
        self.document_path = None
        self.document_viewer.setPixmap(None)
        
    def _onScanBtnClicked(self):
        """ primarily let engine knows user needs recognition. """
        # fire 'scanned' event.
        self._scanned.scanningSignal.emit()

        # UI works stop here.
        # the middle broker Mr.Util will take it from here.
        # UI --> Util <-- LV Engine.

    def _onExportBtnClicked(self):
        """ get what's in review editor and ship it in txt or pdf. """
        # fire 'exported' event.
        pass

    def _onDummyBtnClicked(self):
        """ drop this if you want. just to show sender. """
        btn_txt = self.sender().text()
        UIHelpers(self).popup('OK', btn_txt)
        return
    
    """ overridden event handlers. """

    def closeEvent(self, event):

        # intercept closing event to confirm user choice
        msgbox = UIHelpers(self)
        confirmed = msgbox.askUser('Confirm', 'Are you sure to quit the program ?')

        if confirmed:
            # FileHelper.clean()
            event.accept()
        else:
            event.ignore()


class UIScanningEvent(QtCore.QObject):
    """
    :summary: the ui sanned event signal "event source".
              fires when a user clicks trigger the "scan" action.
    """
    scanningSignal = QtCore.Signal()


class UIHelpers(object):
    """ :summary: collection of reusable UI utility methods."""

    """ A shared across instances field, points to the parent wideget """
    Widget = None

    def __init__(self, widget=None):
        
        if widget:
            Widget = widget

    def askUser(self, title='Question', text=''):
        # simplfied wrap for qt dialog box.
        question = QtGui.QMessageBox.question(UIHelpers.Widget, 
            title, 
            text, 
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, 
            QtGui.QMessageBox.No)

        if question == QtGui.QMessageBox.Yes:
            return True
        else:
            return False

    def popup(self, title='Message', text=''):
        # simplfied wrap for qt messsage box in normal mode.
        QtGui.QMessageBox.information(UIHelpers.Widget, title, text)

    def error(self, title='Error', text='Unexpected error happened'):
        # simplfied wrap for qt messsage box in error mode.
        QtGui.QMessageBox.critical(UIHelpers.Widget, title, text)
