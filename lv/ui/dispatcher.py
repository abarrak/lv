#-*- coding: utf-8 #
"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: ui
"   @module: dispatcher
"   @summary: contains the UI/Engine middle layer.
"   @author: Abdullah Alotaibi.
"""
from lv.common.configuration import LVConfig, LVMode, LVMLSettings, LVFeatureExtractionSettings
from utils import Util

class Dispatcher(object):
  """ arrange communication between UI and engine. """

  def __init__(self, ui, engine_api):
    """
    :param ui: instance of UIShell.
    """
    # keep reference for the two parties for later interaction.
    self.ui_shell = ui
    self.lv_api = engine_api

    # fetch the ui string equivalents of LVConfig.
    self._str_modes, self._str_fe, self._str_ml = Util.strigify_lv_config()

    # register to scanned event in ui.
    self.ui_shell.scanned(self.inform_engine)

  def inform_engine(self):
    # prepare engine-api requirements.
    self._construct_lv_config()

    # inform it. give it a function handle to call back after finishing.
    self.lv_api.preferences = self._config
    self.lv_api.paper_path = self._path
    self.lv_api.verbose = self._vebose

    self.lv_api.scan(self.inform_ui)

  def inform_ui(self, engine_output):
    # a callable helper to hand over results to ui.
    self.ui_shell.recognition_output(engine_output)

  """
  private methods
  """

  def _construct_lv_config(self):
    """ re-constuct config from UI settings to hand over for lv engine. """
    self._config = LVConfig(self._get_user_mode(), self._get_user_fe(), self._get_user_ml())
    self._vebose = self.ui_shell.is_verbose_output
    self._path = self.ui_shell.user_image_path

  def _get_user_mode(self):
    # language preference.
    if self.ui_shell.user_mode == self._str_modes[0]:
      return LVMode.DIGITS_ONLY
    elif self.ui_shell.user_mode == self._str_modes[1]:
      return LVMode.EN_ALPHANUMERIC
    elif self.ui_shell.user_mode == self._str_modes[2]:
      return LVMode.EN_ALPHABET
    elif self.ui_shell.user_mode == self._str_modes[3]:
      return LVMode.AR_ALPHABET
    else:
      raise Exception('Dispatcher exception: unknown mode encounterd.')

  def _get_user_fe(self):
    # feature extraction preference.
    if self.ui_shell.user_fe == self._str_fe[0]:
      return LVFeatureExtractionSettings.RASTER_IMAGE
    elif self.ui_shell.user_fe == self._str_fe[1]:
      return LVFeatureExtractionSettings.HOG
    else:
      raise Exception('Dispatcher exception: unknown feature extraction method encounterd.')

  def _get_user_ml(self):
    # ml method preference.
    if self.ui_shell.user_ml == self._str_ml[0]:
      return LVMLSettings.KNN
    elif self.ui_shell.user_ml == self._str_ml[1]:
      return LVMLSettings.LOGISTIC_REGRESSION
    elif self.ui_shell.user_ml == self._str_ml[2]:
      return LVMLSettings.SGD
    elif self.ui_shell.user_ml == self._str_ml[3]:
      return LVMLSettings.LINEAR_SVM
    elif self.ui_shell.user_ml == self._str_ml[4]:
      return LVMLSettings.SVM_RBF_KERNEL
    elif self.ui_shell.user_ml == self._str_ml[5]:
      return LVMLSettings.SVM_LIN_KERNEL
    elif self.ui_shell.user_ml == self._str_ml[6]:
      return LVMLSettings.SVM_POLY_KERNEL
    elif self.ui_shell.user_ml == self._str_ml[7]:
      return LVMLSettings.DECISION_TREE
    elif self.ui_shell.user_ml == self._str_ml[8]:
      return LVMLSettings.ADABOOST_DECISION_TREE
    elif self.ui_shell.user_ml == self._str_ml[9]:
      return LVMLSettings.RANDOM_FOREST
    elif self.ui_shell.user_ml == self._str_ml[10]:
      return LVMLSettings.EXTRA_TREES
    elif self.ui_shell.user_ml == self._str_ml[11]:
      return LVMLSettings.MLP
    elif self.ui_shell.user_ml == self._str_ml[12]:
      return LVMLSettings.CNN
    else:
      raise Exception('Dispatcher exception: unknown feature extraction method encounterd.')
