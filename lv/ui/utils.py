#-*- coding: utf-8 #
"""
"   LightVision, an offline handwriting recognition system.
"
"   @package: ui
"   @module: utils
"   @summary: contains the  UI utilities and resources.
"   @author: Abdullah Alotaibi.
"""

# app common data hash.
APP_C = {
  
  # resources.

  'text.help': "It's easy !\n\n1. Browse an image from your computer.\n\n2. Choose your preferred \
  machine learning and feauture extraction method.\n\n3. Thne, click scan to start processing.",

  'text.about': "LightVision, an offline handwriting recognition system. \
  \n\nDeveloped by: Abdullah Alotaibi as part of graduaction project. \
  \n\nSupervised by: Dr. Ahemd Bayibani. \n(2016) - CS Department - IMISIU.",

  # config.
  'config.app_title': 'Light Vision',
  # relative to top-level app.py module.
  'config.logo_path': "lv/ui/logo.png",
  'config.icon_path': "lv/ui/icon.png"

}

class Util(object):
  """ the mutual utilities between ui and engine. """

  @staticmethod
  def is_allowed_file(file_name):
    """ check a file againts allowed types. """
    allowed = set(['png', 'jpg', 'jpeg', 'gif', 'bmp'])
    return '.' in file_name and file_name.rsplit('.', 1)[1].lower() in allowed

  @staticmethod
  def strigify_lv_config():
    """ :return: (tuple) of [] consisting of LVConfig options in textual form. """
    return ([
              'Digits Only', 'English Alphanumeric', 'English Alphabet', 'Arabic Alphabet'
            ],
            [
              'Raster Grid', 
              'HOG'
            ],
            [
              'KNN', 
              'Logistic Regression', 
              'SGD Regression',
              'Linear SVM', 
              'SVM with RBF kernel', 
              'SVM with linear kernel', 
              'SVM with polynomial kernel',
              'Decision Tree', 'Ada-boosted Decision Tree', 'Random Forest', 'Extra Trees', 
              'MLP Neural Network',
              'CNN (Convolutional Neural Network)'
            ])